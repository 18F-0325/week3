package com.example.studentportal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

public class MainActivity2 extends AppCompatActivity {
    EditText fname,lname,email,password,cpassword,phone,address;
    Button submit;
    private Button log;
    AwesomeValidation validation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    fname=findViewById(R.id.firstname);
    lname=findViewById(R.id.lastname);
    email=findViewById(R.id.email);
    password=findViewById(R.id.password);
    cpassword=findViewById(R.id.confirm_password);
    phone=findViewById(R.id.phone);
    address=findViewById(R.id.address);
    submit=findViewById(R.id.submit);

    validation= new AwesomeValidation(ValidationStyle.BASIC);
    validation.addValidation(this,R.id.firstname, RegexTemplate.NOT_EMPTY,R.string.invalid_name);
    validation.addValidation(this,R.id.lastname, RegexTemplate.NOT_EMPTY,R.string.invalid_name);
    validation.addValidation(this,R.id.email, Patterns.EMAIL_ADDRESS,R.string.invalid_email);
    validation.addValidation(this,R.id.password, ".{6}",R.string.invalid_pass);
    validation.addValidation(this,R.id.confirm_password,R.id.password,R.string.invalid_cpass);
    validation.addValidation(this,R.id.phone, ".{11}",R.string.invalid_phone);

    submit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(validation.validate())
            {

                log = (Button) findViewById(R.id.submit);
                log.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        openactivity();
                    }
                });
            }
            else
            {
                Toast.makeText(getApplicationContext(),"User not Registered!",Toast.LENGTH_SHORT).show();
            }
        }
    });



    }
    public void openactivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(),"User Registered!",Toast.LENGTH_SHORT).show();

    }
}